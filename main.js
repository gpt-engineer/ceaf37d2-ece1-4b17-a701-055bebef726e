document.getElementById("addProject").addEventListener("click", function () {
  var project = document.createElement("div");
  project.className = "bg-gray-200 p-4 slide-in";
  project.innerHTML = `
        <h3 class="text-xl font-bold mb-2">New Project</h3>
        <p>This is a new project.</p>
    `;
  document.getElementById("projects").appendChild(project);
});
